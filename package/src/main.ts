import { app, BrowserWindow } from "electron";
import * as path from "path";
const fs = require("fs");

require('electron-reload')(__dirname, {
  electron: path.join(__dirname, '../node_modules', '.bin', 'electron'),
  awaitWriteFinish: true,
});

export function returnDefaultWindow(){
  const mainWindow = new BrowserWindow({
    height: 600,
    width: 800,

    webPreferences: {
      nodeIntegration: true,
      
      contextIsolation: false,
      preload: path.join(__dirname, "preload.js"),
    },
  });
  return mainWindow
}
function createWindow() {
  
  const mainWindow = returnDefaultWindow()
  mainWindow.loadFile(path.join(__dirname, "../public/index.html"));
  mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", () => {
  updateJson().then(uwu => {
    fs.writeFileSync("./public/q.json", uwu)
  })
  createWindow();
  app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
    
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

const net = require("net");
async function updateJson() {
  var query = {
    query: "fetch", ints: 0
  }
  return new Promise(resolve => {

    var socket = new net.Socket();
    var send30 = JSON.stringify(query)
    console.log(send30)
    socket.setEncoding('utf8');
    socket.connect(12345, "127.0.0.1", function () {
      socket.write(send30 + "\r\L" + "\n")
      let dataa: string[] = []
      socket.on('data', function (data : string) {
        dataa.push(data)
      })
      socket.on('end', function () {
        resolve(dataa.join(''))
      })
    })
  })
}
// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
