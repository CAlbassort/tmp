import strutils, marshal, nimpy, re
import NimRecipe, ColdFusionlib
let recipes = to[seq[Recipe]](readFile("output.json"))
var invalidRecipe : seq[string]
let python = pyImport("nimServerStuff")
proc listOfNouns(name: string) : seq[string] =
  return python.listOfNouns(name).to(seq[string])

for x in readFile("ColdFusionLogs/October.19.2021.04:31:57-04:00.txt").split("\n"):
    if not x.contains "exception":
        invalidRecipe.add(x.split("]: ")[1].split(" Failed")[0])
for x in recipes:
    if x.name == "Caramelized Onion Galette":
        echo x.name
        try:
            discard packArchive(x)
            echo "\n"    
        except OutOfMemDefect:
            continue
#for x in recipes:
    