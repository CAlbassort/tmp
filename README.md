Demonstration project written in svelte with NIM,TS and JS

the usual startup
```bash
npm install
```
We also require nim <=1.6.0, python <= 3.4, nimpy, textblob, and spaCy
```bash
#debian apt-like package managers
sudo apt-get install nim
#arch, Pacman-like package managers
sudo pacman -S nim
#void linux
xbps-install nim
```
```bash
nimble install nimpy
pip3 install textblob spacy
./NimRecipe/compile.sh
```
## Building the dataset:

run NimRecipe and it will crawl the new york time servers; compiling a unprocessed Recipe dataset which will be saved for processing in ColdFusionlib; which will ping the https://www.foodrepo.org servers.
You must have a token, put it in NimRecipe/token.txt.
After that, you can compile it by running ColdFusion; coincidentally it also runs the server which updates electron upon reload.

There is no ID system so small packets cannot be sent, it will transmit the full data, which can be quite big; good thing its not over the internet. This, if you harddrive or ssd is on its last life might cause damage so this is a pretty big todo for me.

## Electron:



The git include has a working dataset so there is no real need to make your own. It will probably identical or near identical to the one provided. 



[CC0 1.0 (Public Domain)](LICENSE.md)
