var buffer = "sdrf;glk"
importScripts("fastdice.js")
var bufferdate = 0
let fields = ["carbohydrates", "fat", "protein", "sodium", "sugars", "cholesterol", "fiber"]
let fieldcompanions = {0 : "calories", 1 : "calories", 2 : "calories", 3 : "grams", 4 : "grams", 5 : "mg", 6 : "grams"}

function getSum(a){
    return (a.reduce(function(a,b){return a+b}))
}
async function queryServer(search, big = true) {
    console.log("entered")
    var final = []
    return new Promise(resolve => {
        var key = []

        for (let i in jsonlookup) {
            var coefficient = dice(search, jsonlookup[i]["name"])
            var img = jsonlookup[i]["imgurl"]
            if (img.length < 13){
                //this sets the default image if there is not likely to be one
                img = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-zqUA_lTBN0HNgeNisDcJdfPRhaLHnww1dQ&usqp=CAU"
            }
            //this makes key a tuple of this stuff
            key.push(        
                [coefficient, jsonlookup[i]["name"], jsonlookup[i]["author"], 
                    jsonlookup[i]["url"], jsonlookup[i]["dateadded"], img, jsonlookup[i]["ingredients"]]
            )
            
        }

        key.sort(function (a, b) { return b[0] - a[0]; })
        //makes it so small searches do less operations.
        if (big == true){
                                //the != sets the total amount of entries.
                                //added from key
            for (let i = 0; final.length != 40; i++) {
                var output = []
                var calories = [[],[],[],[],[],[],[]]
                var iterate = JSON.parse(key[i][6])

                var currentiterator 

                console.log(iterate)
                //so this stuff compiles the text. This mostly is computated from the data done in coldfusionlib.nim
                for (var x in iterate){
                    currentiterator = iterate[x]
                    output.push(currentiterator["name"] )

                    for(var y in fields){
                        calories[y].push(currentiterator["makeup"][fields[y]])
                    }
                } 
                var total = []
                var totalstotal = []
                for (var x in calories){
                    try{
                        if (x<=2){
                            
                            total.push(( getSum(calories[x]) * currentiterator["mass"]) )
                        }
                        output.push(fields[x] +": "+ ( getSum(calories[x]) * currentiterator["mass"]) + " " + fieldcompanions[x])         
                        totalstotal.push(getSum(calories[x]) * currentiterator["mass"])
                    }
                    catch (error) { continue }
                }

                try{output.push("total calories:" + getSum(total))
                    totalstotal.push(getSum(total))
            }
                catch (error) { }

                var id = {"name" : key[i][1], 
                    "author" : key[i][2], "url" : key[i][3],
                    "dateadded" : key[i][4], "imgurl" : key[i][5], "ingredients" : output.join("<br>"), 
                    "calories" : totalstotal}
                //sorts out duplicates theoretically, however it doesn't always work in practice.
                //must add a more complex check here :)
                if (!final.includes(id)) {
                    final.push(id)
                }  
            }

            bufferdate = Date.now()
            resolve(JSON.stringify(final))
        }
        else{ resolve(key[0]) }

    })
}

//is a smaller search
async function autoupdate(value){
    if (value != buffer) {

        if (value.length > 2) {
            ping = []
            var sdo = await queryServer(value, false)
            //denotes this as a small baby
            self.postMessage(sdo+="≈")
            console.log("sent")
            buffer = value
        }
    }
}



var recieved = 0  
     //for some reason i cant edit the var before its in the scope.
self.onmessage = function (value) {
    var value = value["data"]
    if (recieved == 0){
        jsonlookup = JSON.parse(value)
        recieved = 1
        console.log("json recieved")
    }
            //this checks if its a smaller quest(denoted by the squigly equals)
                                    //creates buffer so it doesn't overflow and crash
    else if (value.includes("≈") && (Date.now()/1000) - (bufferdate/1000) > 0.5){
        autoupdate(value.slice(0, -1))
    }

    else{
        queryServer(value).then(returned => {
            self.postMessage(String(returned))})
    }
    
}
   
