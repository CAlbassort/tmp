importScripts("fastdice.js")
self.onmessage = function (value) {
    let input = JSON.parse(value["data"])
    //for each array i nthe master array
    var outputsort = []
    const search = input["search"]
    const inputarray = input["recipe"]
    for (var list in inputarray){
        const current = inputarray[list]
        for (var recipe in current){
            const lower = current[recipe]
            outputsort.push([lower[0], lower[1], dice(search, lower[1])])
        }
    }
    outputsort.sort(function (a, b) { return b[2] - a[2]; })
    organized = [[]]

    for (let entry in outputsort){
        if (organized.at(-1).length == 9) { organized.push([]) }
        organized.at(-1).push([outputsort[entry][0], outputsort[entry][1]])
    }
    console.log(organized)
    self.postMessage(JSON.stringify(organized))
    return
}  
